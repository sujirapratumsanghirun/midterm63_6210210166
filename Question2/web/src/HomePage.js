
import './index.css'
import Grid from '@material-ui/core/Grid';
function HomePage(){
    return (
        <div>
            <div className="Head">
            <h1>PUBG MOBILE </h1>
            </div>
            <Grid container spacing={1}>
            <Grid container item xs={4} spacing={3}>
                <h2>"รูปแบบของเกมคือเราจะต้องเอาตัวรอดให้ได้จนเป็นคนหรือทีมสุดท้ายจากทั้งหมด 100 คน บนแผนที่ใหญ่ๆ ที่จะถูกบีบพื้นที่เล่นเข้ามาเรื่อยๆ"</h2>
            </Grid>
            <Grid container item xs={4} spacing={3}>
            <h2 className="text">"1ทีมมี4คน"</h2>
            </Grid>
            <Grid container item xs={4} spacing={3}>
            <h2>"สุดยอดทีมที่คว้ารางวัลมากมาย<br/>RRQ ATHENA"</h2>
            </Grid>
            <Grid container item xs = {6} spacing={3}>
                <h2>"Playerunknown’s Battlegrounds"</h2>
            </Grid>
            <Grid container item xs = {6} spacing={3}>
                <h2>"สมาชิกทีม RRQ ATHENA<br/>G9,D2E,Earny007,Senior007,BEER11"</h2>
            </Grid>
            <Grid container item xs = {6} spacing={3}>
                <img src="https://www.khaosod.co.th/wpapp/uploads/2020/11/126367406_4060598983967405_11526411032848016_o-1.jpg" alt="Italian Trulli" className="FirstPhoto"/>
            </Grid>
            <Grid container item xs = {6} spacing={3}>
            <img src="https://www.khaosod.co.th/wpapp/uploads/2020/11/121971562_774160096767311_4017633522837853967_o.jpg" className="SecPhoto" alt="Italian Trulli"/>

            </Grid>
            </Grid>
        </div>
    );
}
export default HomePage;