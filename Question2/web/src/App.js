import logo from './logo.svg';
import './App.css';
import Homepage from './HomePage';
import Program1 from './Program1';
import Program2 from './Program2';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button'

import { BrowserRouter as Router, Route, Switch, Link} from "react-router-dom";

function App() {
  return (
    <Router>
    <AppBar position="static">
       <Toolbar>
         <Button><Link to="/">หน้าแรก </Link></Button>
         <Button><Link to="/sec">หน้าสอง </Link></Button>
         <Button><Link to="/third">หน้าสาม </Link></Button>
       </Toolbar>
     </AppBar>
    <div>
     <Switch>
         <Route exact path="/"><Homepage /></Route>
         <Route path="/sec"><Program1 /></Route>
         <Route path="/third"><Program2 /></Route>
       </Switch>
     </div>
  </Router>
  );
}

export default App;
