import logo from './logo.svg';
import './App.css';
import HomePage from './HomePage'
import Program1 from './Program1'
import Program2 from './Program2'

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
}from'react-router-dom';
import {
  AppBar,
  Button,
  Toolbar,
  
}from '@materail-ui/core';
import HomeIcon from '@material-ui/icons/Home';

function App() {
  return (
    <Router>

      <AppBar>
      <Toolbar>
      <Link to="/"><Button><HomeIcon /></Button> </Link>
      <Link to="/"> <Button> Home </Button> </Link>
      <Link to="/news"> <Button>News</Button> </Link>
      <Link to="/about"> <Button>About</Button> </Link>
      </Toolbar>
      </AppBar>

      <div style={{marginTop:100}}>
      
      <Switch>
        <Route exact path="/">
            <HomePage />
        </Route>

        <Route exact path="/Program1">
              <Program1 />
        </Route>

        <Route exact path="/Program2">
              <Program2/>
        </Route>

       </Switch>
      </div>

    </Router>

  );
}

export default App;
